package com.xenoterracide.logging;

import java.util.function.Supplier;

public interface Logger {

    void trace( Throwable throwable );

    void debug( Throwable throwable );

    void info( Throwable throwable );

    void warn( Throwable throwable );

    void error( Throwable throwable );

    void trace( String template, ArgSupplier... suppliers );

    void debug( String template, ArgSupplier... suppliers );

    void info( String template, ArgSupplier... suppliers );

    void warn( String template, ArgSupplier... suppliers );

    void error( String template, ArgSupplier... suppliers );

    interface ArgSupplier extends Supplier<String> {
    }
}

package com.xenoterracide.logging;

import java.util.function.Supplier;
import java.util.stream.Stream;

import org.slf4j.spi.LocationAwareLogger;

final class SLF4JLoggerFacade implements Logger {

    private final org.slf4j.Logger logger;

    SLF4JLoggerFacade( final org.slf4j.Logger logger ) {
        this.logger = logger;
    }

    private static Object[] fromSuppliers( final Supplier<String>[] suppliers ) {
        Stream<Supplier<String>> stream = suppliers == null ? Stream.empty() : Stream.of( suppliers );
        return stream.map( Supplier::get ).toArray();
    }

    private void log( final int level, final String fmt, final Throwable ex, final Supplier<String>[] sup ) {
        if ( logger instanceof LocationAwareLogger ) {
            LocationAwareLogger lag = (LocationAwareLogger) logger;
            String fqcn = this.getClass().getName();
            lag.log( null, fqcn, level, fmt, fromSuppliers( sup ), ex );
        }
    }

    @Override
    public void trace( final Throwable throwable ) {
        if ( logger instanceof LocationAwareLogger ) {
            this.log( LocationAwareLogger.TRACE_INT, "{}", throwable, null );
        }
        else {
            logger.trace( "{}", throwable );
        }
    }

    @Override
    public void debug( final Throwable throwable ) {
        if ( logger instanceof LocationAwareLogger ) {
            this.log( LocationAwareLogger.DEBUG_INT, "{}", throwable, null );
        }
        else {
            logger.debug( "{}", throwable );
        }
    }

    @Override
    public void info( final Throwable throwable ) {
        if ( logger instanceof LocationAwareLogger ) {
            this.log( LocationAwareLogger.INFO_INT, "{}", throwable, null );
        }
        else {
            logger.info( "{}", throwable );
        }
    }

    @Override
    public void warn( final Throwable throwable ) {
        if ( logger instanceof LocationAwareLogger ) {
            this.log( LocationAwareLogger.WARN_INT, "{}", throwable, null );
        }
        else {
            logger.warn( "{}", throwable );
        }
    }

    @Override
    public void error( final Throwable throwable ) {
        if ( logger instanceof LocationAwareLogger ) {
            this.log( LocationAwareLogger.ERROR_INT, "{}", throwable, null );
        }
        else {
            logger.error( "{}", throwable );
        }
    }

    @Override
    public void trace( final String template, final ArgSupplier... suppliers ) {
        if ( logger.isTraceEnabled() ) {
            if ( logger instanceof LocationAwareLogger ) {
                this.log( LocationAwareLogger.TRACE_INT, template, null, suppliers );
            }
            else {
                logger.trace( template, fromSuppliers( suppliers ) );
            }
        }
    }

    @Override
    public void debug( final String template, final ArgSupplier... suppliers ) {
        if ( logger.isDebugEnabled() ) {
            if ( logger instanceof LocationAwareLogger ) {
                this.log( LocationAwareLogger.DEBUG_INT, template, null, suppliers );
            }
            else {
                logger.debug( template, fromSuppliers( suppliers ) );
            }
        }
    }

    @Override
    public void info( final String template, final ArgSupplier... suppliers ) {
        if ( logger.isInfoEnabled() ) {
            if ( logger instanceof LocationAwareLogger ) {
                this.log( LocationAwareLogger.INFO_INT, template, null, suppliers );
            }
            else {
                logger.info( template, fromSuppliers( suppliers ) );
            }
        }
    }

    @Override
    public void warn( final String template, final ArgSupplier... suppliers ) {
        if ( logger.isWarnEnabled() ) {
            if ( logger instanceof LocationAwareLogger ) {
                this.log( LocationAwareLogger.WARN_INT, template, null, suppliers );
            }
            else {
                logger.warn( template, fromSuppliers( suppliers ) );
            }
        }
    }

    @Override
    public void error( final String template, final ArgSupplier... suppliers ) {
        if ( logger.isErrorEnabled() ) {
            if ( logger instanceof LocationAwareLogger ) {
                this.log( LocationAwareLogger.ERROR_INT, template, null, suppliers );
            }
            else {
                logger.error( template, fromSuppliers( suppliers ) );
            }
        }
    }
}

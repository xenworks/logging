package com.xenoterracide.logging;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

final class LoggerFactory {

    private static final ThreadLocal<Map<Package, Logger>> LOGGERS = ThreadLocal.withInitial( HashMap::new );

    static Logger getInstance( final Object o ) {
        Objects.requireNonNull( o );
        return LOGGERS.get().computeIfAbsent( o.getClass().getPackage(), LoggerFactory::getLogger );
    }

    private static Logger getLogger( final Package pkg ) {
        Objects.requireNonNull( pkg );
        return new SLF4JLoggerFacade( org.slf4j.LoggerFactory.getLogger( pkg.getName() ) );
    }
}

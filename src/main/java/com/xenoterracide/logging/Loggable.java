package com.xenoterracide.logging;

public interface Loggable {

     default Logger log() {
         return LoggerFactory.getInstance( this );
     }
}

package com.xenoterracide.logging;

import java.util.function.Predicate;

import org.junit.After;
import org.junit.Test;
import uk.org.lidalia.slf4jext.Level;
import uk.org.lidalia.slf4jtest.LoggingEvent;
import uk.org.lidalia.slf4jtest.TestLogger;
import uk.org.lidalia.slf4jtest.TestLoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;

public class LoggableTest {
    private final TestLogger logger = TestLoggerFactory.getTestLogger( this.getClass().getPackage().getName() );
    private final Loggable loggable = new Loggable() {
    };
    private final Logger log = loggable.log();

    @Test
    public void same() throws Exception {
        Logger log0 = loggable.log();
        assertThat( log0 ).isInstanceOf( SLF4JLoggerFacade.class );
        Logger log1 = loggable.log();
        Loggable loggable1 = new Loggable() {
        };
        assertThat( log1 ).isSameAs( log0 );
        assertThat( loggable1.log() ).isSameAs( log0 );
    }

    @Test
    public void simple() throws Exception {

        String msg = "test";
        log.trace( msg );
        log.debug( msg );
        log.info( msg );
        log.warn( msg );
        log.error( msg );

        assertThat( logger.getAllLoggingEvents() )
            .extracting( LoggingEvent::getMessage )
            .allMatch( Predicate.isEqual( msg ) );

        assertThat( logger.getAllLoggingEvents() )
            .extracting( LoggingEvent::getLevel )
            .containsExactly( Level.TRACE, Level.DEBUG, Level.INFO, Level.WARN, Level.ERROR );
    }

    @Test
    public void suppliers() throws Exception {
        Integer one = 1;
        String msg = "test {}";
        log.trace( msg, one::toString );
        log.debug( msg, one::toString );
        log.info( msg, one::toString );
        log.warn( msg, one::toString );
        log.error( msg, one::toString );

        assertThat( logger.getAllLoggingEvents() )
            .flatExtracting( LoggingEvent::getArguments )
            .allMatch( Predicate.isEqual( one.toString() ) );

        assertThat( logger.getAllLoggingEvents() )
            .extracting( LoggingEvent::getMessage )
            .allMatch( Predicate.isEqual( msg ) );

        assertThat( logger.getAllLoggingEvents() )
            .extracting( LoggingEvent::getLevel )
            .containsExactly( Level.TRACE, Level.DEBUG, Level.INFO, Level.WARN, Level.ERROR );
    }

    @Test
    public void exceptions() throws Exception {
        Exception exception = new RuntimeException( "test" );

        log.trace( exception );
        log.debug( exception );
        log.info( exception );
        log.warn( exception );
        log.error( exception );


        assertThat( logger.getAllLoggingEvents() )
            .extracting( LoggingEvent::getThrowable )
            .extracting( com.google.common.base.Optional::get )
            .allMatch( Predicate.isEqual( exception ) );

        assertThat( logger.getAllLoggingEvents() )
            .extracting( LoggingEvent::getLevel )
            .containsExactly( Level.TRACE, Level.DEBUG, Level.INFO, Level.WARN, Level.ERROR );
    }

    @After
    public void clearLoggers() {
        logger.clearAll();
    }

}
